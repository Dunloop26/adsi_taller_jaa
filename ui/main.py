def imprimir_menu(opciones : list, encabezado: str = "",pie: str = "") -> None:

    # Imprimo el encabezado
    print(encabezado)

    # Imprimo las opciones
    longitud_opciones = len(opciones)
    for i in range(longitud_opciones):
        opcion = opciones[i]
        print(f"{i+1}: {opcion}")

    # Imprimo el pie de página
    print(pie)

def elegir_opciones(opciones: list, mensaje: str = "Por favor, elija una opción: "):
    # Obtengo la entrada del usuario
    entrada_usuario = input(mensaje)
    print()

    # Convierto el valor a un número con la opción
    opcion = -1
    try:
        opcion = int(float(entrada_usuario))-1
    except ValueError:
        print(f"No se ha podido convertir el valor \"{entrada_usuario}\" a un número")

    # Compruebo que la elección no esté por fuera del rango
    if opcion < 0 or opcion >= len(opciones):
        print("No se ha ingresado una opción dentro del rango de opciones")
        return None

    # Retorno una tupla con la opción elegida y su índice
    return opcion
