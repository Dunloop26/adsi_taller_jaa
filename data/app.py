class AppConfig:
    def __init__(self, limite_creditos: float = 80000, clientes: list = []):
        self.limite_creditos: float = limite_creditos
        self.clientes: list = clientes
