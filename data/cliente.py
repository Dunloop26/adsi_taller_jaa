class Cliente:
    def __init__(self, nombre: str, saldo: float, movil: str):
        self.nombre: str = nombre
        self.saldo: float = saldo
        self.movil: str = movil
        self.activo: bool = True

    def __repr__(self) -> str:
        return f"{self.nombre} - {self.movil} - ${self.saldo}"
