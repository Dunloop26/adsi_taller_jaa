from data import Cliente, AppConfig
import re

def crear_cliente(app_config: AppConfig):
    """ Solicita los datos de nombre, saldo pendiente y número de movil para realizar la creación de un nuevo cliente"""
    
    # Creo la función de validación para el saldo
    def obtener_saldo(limite_creditos: float):
        while (True):
            try:
                # Convierto la entrada del usuario a float
                saldo = float(input("Saldo del cliente: "))
                
                # Si el saldo supera el límite de créditos
                if saldo > limite_creditos:
                    # Notifico al usuario y pregunto
                    print(f"El valor de saldo actual supera el límite de créditos: {limite_creditos}")
                    decision = input("¿Desea ingresar nuevamente el valor para el saldo? (S/n): ").lower()
                    # Si la decisión no es NO
                    if not decision == "n":
                        # Continuo a corregir el saldo
                        continue

                return saldo
            except ValueError:
                print("No ha ingresado un valor para el saldo correcto. Por favor, ingrese solo números")

    # Creo la función para validar que el número de movil no esté repetido
    def movil_valido(movil: str, clientes: list):
        # Es válido
        # Si el movil existe
        # Si el movil NO son espacios vacios
        # Si el movil son dígitos

        # Uno el número de móvil quitando los espacios
        movil = "".join(re.findall(r'[0-9]*', movil))
        return movil and not movil.isspace() and movil.isdigit()


    def movil_registrado(movil: str, clientes: list):
        # Compruebo que el número de movil no exista
        matches = list(cliente for cliente in clientes if cliente.movil == movil)
        return len(matches) != 0 # Si no hay coincidencias del número

    # Creo el ciclo para crear el cliente
    while True:
        print()
        print("Por favor, ingrese los datos solicitados a continuación.\n")
        
        # Ingreso el nombre del cliente
        while True:
            nombre = input("Nombre del cliente: ")
            # Si el nombre existe y no es un espacio en blanco
            if nombre and not nombre.isspace():
                break
            else:
                print("Por favor, use un nombre válido para el nombre de cliente. Debe ingresar al menos un caracter.")

        # Obtengo el saldo con límite de créditos
        saldo = obtener_saldo(app_config.limite_creditos)
        
        # Ingreso el número del movil
        while True:
            movil = input("Número de movil: ")
            # Verifico que el número de movil no esté ya registrado
            if not movil_valido(movil, app_config.clientes):
                print(f"El número de movil {movil} no es un número de movil válido")
            elif movil_registrado(movil, app_config.clientes):
                print(f"El número de movil {movil} ya está registrado")
            else:
                break

        # Agrego el cliente a la lista de registros
        app_config.clientes.append(Cliente(nombre, saldo, movil))

        print(f"Se ha registrado el cliente \"{nombre}\" correctamente")
        break
