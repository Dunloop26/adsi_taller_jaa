from ui.main import elegir_opciones, imprimir_menu
from data.app import AppConfig

def activacion_cliente(app_config: AppConfig):

    # Obtengo la lista de los clientes
    clientes = app_config.clientes
    
    # Si no hay clientes
    if len(clientes) == 0:
        print("No hay clientes para activar o desactivar")
        return # Salgo de la opción

    while True:
        # Creo una lista con el estado de activación
        lista_activacion = [f"{cliente.nombre} - {'ACTIVO' if cliente.activo else 'INACTIVO'}" for cliente in clientes]
        
        # Agrego la opción de salir
        lista_activacion.append("Salir")

        # Mientras el cliente elegido no sea el correcto
        opcion_elegida = None
        while opcion_elegida == None:
            # Obtengo el menú de clientes
            imprimir_menu(lista_activacion, encabezado="Lista de clientes: ")
            opcion_elegida = elegir_opciones(lista_activacion)
        
        # Si elijo la opción para salir
        if opcion_elegida == len(lista_activacion)-1:
            break
        else:
            # Invierto el estado de activación
            cliente_elegido = clientes[opcion_elegida]
            cliente_elegido.activo = not cliente_elegido.activo

            # Notifico al usuario
            print(f"El estado del cliente {cliente_elegido.nombre} ahora es: {'ACTIVO' if cliente_elegido.activo else 'INACTIVO'}")
