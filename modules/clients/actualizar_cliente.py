from ui.main import elegir_opciones, imprimir_menu
from data.app import AppConfig

def actualizar_cliente(app_config: AppConfig):
    # Obtengo la lista de clientes
    clientes = app_config.clientes

    # Filtro solamente los clientes activos
    clientes_activos = [cliente for cliente in clientes if cliente.activo]
    
    # Si hay clientes a actualizar
    if len(clientes_activos) > 0:

        # Creo el menu de opciones para las modificaciones
        opciones = ["Modificar nombre", "Modificar saldo", "Salir"]
        OPCION_NOMBRE = 0
        OPCION_SALDO = 1
        OPCION_SALIR = 2
 
        # Agrego la opción Salir para los clientes
        opciones_clientes = clientes_activos + ["Salir"]

        cliente_elegido = None
        while (cliente_elegido == None):
            print("-"*80)
            
            # Listamos los clientes de la aplicación
            imprimir_menu(opciones_clientes, encabezado="Lista de clientes:")
            cliente_elegido = elegir_opciones(opciones_clientes)
        
        # Si la opción que elegí es la opción salir
        if cliente_elegido == len(opciones_clientes) - 1:
            return

        opcion_modificacion_elegida = None
        while(opcion_modificacion_elegida == None):
            print("-"*80)
            imprimir_menu(opciones, encabezado="Opciones de edición:")
            opcion_modificacion_elegida = elegir_opciones(opciones)

        # Obtengo el cliente elegido
        cliente = clientes[cliente_elegido]

        # Si elijo la opción de salida
        if opcion_modificacion_elegida == OPCION_SALIR:
            return
        elif opcion_modificacion_elegida == OPCION_NOMBRE:
            # Pido el nombre del cliente
            nombre_cliente = cliente.nombre
            print(f"Nombre del cliente registrado: {nombre_cliente}")
            nuevo_nombre_cliente = input("Ingrese el nuevo nombre del cliente: ")

            # Si el nuevo nombre de cliente está vacío
            if nuevo_nombre_cliente.isspace():
                nuevo_nombre_cliente = nombre_cliente
                print("Se ha ingresado un valor vacío para el nombre del cliente")

            # Registro el nombre del cliente
            cliente.nombre = nuevo_nombre_cliente
        elif opcion_modificacion_elegida == OPCION_SALDO:
            # Pido el saldo del cliente
            saldo = cliente.saldo
            print(f"Saldo del cliente registrado: {saldo}")
            
            # Defino el bucle para validar el saldo
            nuevo_saldo = None
            saldo_aceptado = False
            while not nuevo_saldo or not saldo_aceptado:
                print("-"*80)
                entrada_saldo = input("Ingrese el nuevo saldo para el cliente: ")
                try:
                    nuevo_saldo = float(entrada_saldo)
                except ValueError:
                    print(f"No se puede usar \"{entrada_saldo}\" para el saldo del cliente")
                except KeyboardInterrupt:
                    nuevo_saldo = saldo
                
                # Si el saldo es aceptado con los límites
                if nuevo_saldo and nuevo_saldo > app_config.limite_creditos:
                    print(f"Se ha ingresado un valor superior al límite de créditos permitido. {app_config.limite_creditos}")
                    decision_cambiar_saldo = input(f"¿Desea modificar el saldo ingresado \"{nuevo_saldo}\"? (S/n): ")
                    
                    # Si la decisión es "no"
                    if decision_cambiar_saldo == "n":
                       saldo_aceptado = False
                else:
                    saldo_aceptado = True

            # Asigno el saldo al usuario
            cliente.saldo = nuevo_saldo

            # Notifico al usuario
            print(f"Se ha modificado el saldo del cliente {cliente.nombre} a {nuevo_saldo} satisfactoriamente.")
    else:
        print("No hay clientes para actualizar")
