from data import AppConfig
from ui import imprimir_menu, elegir_opciones
import modules.clients 

def main():
    # Establezco el menú de opciones
    opciones = ["Crear un nuevo cliente", "Actualizar un cliente existente", "Desactivar o activar un cliente", "Definir límite de saldo", "Salir"] 
    OPCION_CREAR_CLIENTE = 0
    OPCION_ACTUALIZAR_CLIENTE = 1
    OPCION_ACTIVAR_CLIENTE = 2
    OPCION_LIMITE_SALDO = 3
    OPCION_SALIR = 4

    # Variables de proyecto
    app_config = AppConfig(limite_creditos=80000, clientes=[])

    # Ciclo principal del programa
    while True:
        # Defino la opción como 'no elegida'
        indiceOpcion = None

        # Ejecuto un ciclo hasta que haya una opción válida
        while indiceOpcion == None:

            print("-"*80)
            print(f"El límite de saldo actual es: {app_config.limite_creditos}")
            # Imprimo el menú de opciones
            imprimir_menu(opciones)

            try:
                # Obtengo el valor de la opción y el índice de la opción
                indiceOpcion = elegir_opciones(opciones)

            except KeyboardInterrupt:
                print()
                print("x"*80)
                print("Se ha cancelado la operación de elección")
                # Se define la opción salir como la opción por defecto
                indiceOpcion = OPCION_SALIR

        # Elijo la opción de la lista de opciones
        opcion = opciones[indiceOpcion]

        print("-"*80)
        # Si la opción es salir
        if indiceOpcion == OPCION_SALIR:
            print("Ha salido del programa")
            exit()
        else:
            print(f"Su opcion es: {opcion}")

            # Si elige la opción para registrar un límite de saldo
            if indiceOpcion == OPCION_LIMITE_SALDO:
                app_config.limite_creditos = float(input("Por favor, ingrese el nuevo límite de saldo: "))
            elif indiceOpcion == OPCION_CREAR_CLIENTE: # Si elige la opción para registrar un nuevo cliente
                try:
                    modules.clients.crear_cliente(app_config)
                except KeyboardInterrupt:
                    print()
                    print("x"*80)
                    print("\nSe ha cancelado la operación de creación de cliente")
            elif indiceOpcion == OPCION_ACTUALIZAR_CLIENTE:
                try:
                    modules.clients.actualizar_cliente(app_config)
                except KeyboardInterrupt:
                    print()
                    print("x"*80)
                    print("\nSe ha cancelado la operación de actualización de cliente")
            elif indiceOpcion == OPCION_ACTIVAR_CLIENTE:
                try:
                    modules.clients.activacion_cliente(app_config)
                except KeyboardInterrupt:
                    print()
                    print("x"*80)
                    print("\nSe ha cancelado la operación de activación de cliente")


if __name__ == "__main__":
    main()
